package com.openyoung.globant.chatbotapp.views;

import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.openyoung.globant.chatbotapp.ChatBotApplication;
import com.openyoung.globant.chatbotapp.listeners.AIListener;

import ai.api.android.AIDataService;


/**
 * Created by sandeep.kharat on 8/21/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements AIListener {

    private ChatBotApplication app;

    private static final long PAUSE_CALLBACK_DELAY = 500;

    private final Handler handler = new Handler();
    private BaseActivity thisActivity;
    public AIDataService aiDataService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (ChatBotApplication) getApplication();
        aiDataService = new AIDataService(this, app.getAIConfiguration());
        thisActivity = this;
    }


}
