package com.openyoung.globant.chatbotapp.utils;

import android.text.TextUtils;
import android.util.Log;

import com.openyoung.globant.chatbotapp.models.Payload;

/**
 * Created by sandeep.kharat on 8/21/2017.
 */

public class StringUtils {
    private static StringUtils _INSTANCE = null;

    private StringUtils() {

    }

    public static StringUtils getInstance() {
        if (null == _INSTANCE) {
            synchronized (StringUtils.class) {
                if (null == _INSTANCE) {
                    _INSTANCE = new StringUtils();
                }
            }
        }
        return _INSTANCE;
    }

    public Payload getPayLoad(String strCSV) {
        Payload payload;
        try {
            strCSV = strCSV.trim();
            payload = new Payload();
            if (!TextUtils.isEmpty(strCSV) && strCSV.contains(":")) {
                String strPrimary[] = strCSV.split(",");
                String[] strSub;
                for (String string : strPrimary) {
                    strSub = string.split(":");
                    Log.d(strSub[0], strSub[1]);
                    if (strSub[0].startsWith("component")) {
                        payload.setComponent(strSub[1]);
                    } else if (strSub[0].startsWith("message")) {
                        payload.setMessage(strSub[1]);
                    } else if (strSub[0].startsWith("text")) {
                        payload.setMessageType(strSub[1]);
                    }
                }
            } else if (!TextUtils.isEmpty(strCSV)) {
                payload.setComponent("text");
                payload.setMessageType(strCSV);
                payload.setMessage(strCSV);
                return payload;
            }
        } catch (Exception exception) {
            payload = null;
            exception.printStackTrace();
        }
        return payload;
    }
}
