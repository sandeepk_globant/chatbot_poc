package com.openyoung.globant.chatbotapp.utils;

import android.os.AsyncTask;

import com.openyoung.globant.chatbotapp.listeners.AIListener;

import ai.api.AIServiceException;
import ai.api.android.AIDataService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

/**
 * Created by sandeep.kharat on 8/21/2017.
 */

public class ChatRequestAsyncTask extends AsyncTask<AIRequest, Void, AIResponse> {
    private final AIListener listener;
    private final AIDataService aiDataService;

    @Override
    protected AIResponse doInBackground(AIRequest... requests) {
        final AIRequest request = requests[0];
        try {
            final AIResponse response = aiDataService.request(request);
            return response;
        } catch (AIServiceException e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(AIResponse aiResponse) {
        if (aiResponse != null) {
            // process aiResponse here
            this.listener.onResult(aiResponse);
        }else{
            this.listener.onError(new AIError(new AIServiceException("No response")));
        }
    }

    /**
     *
     * @param listener
     * @param aiDataService
     */
    public ChatRequestAsyncTask(AIListener listener, AIDataService aiDataService) {
      this.listener=listener;
        this.aiDataService=aiDataService;
    }
}
