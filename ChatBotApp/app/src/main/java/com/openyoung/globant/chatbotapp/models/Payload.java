package com.openyoung.globant.chatbotapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sandeep.kharat on 8/21/2017.
 */

public class Payload {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ui_component")
    @Expose
    private String component;
    @SerializedName("message_type")
    @Expose
    private String messageType;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", component='" + component + '\'' +
                ", messageType='" + messageType + '\'' +
                '}';
    }
}
