package com.openyoung.globant.chatbotapp;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import ai.api.android.AIConfiguration;

/**
 * Created by sandeep.kharat on 8/21/2017.
 */

public class ChatBotApplication extends Application {
    private AIConfiguration _AIConfiguration;

    @Override
    public void onCreate() {
        super.onCreate();
        initAI();
    }

    private void initAI() {
        //Sagar: 2397e3a22a0a4bd5a166c075a188016f
        //Sandeep: e85fd7f7defe48dda3005c0e0816fbd9
        //OY-New: f0787ca8c13146b29ff249424485ab15

        _AIConfiguration = new AIConfiguration("f0787ca8c13146b29ff249424485ab15",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
    }

    public AIConfiguration getAIConfiguration() {
        return _AIConfiguration;
    }


}
