package com.openyoung.globant.chatbotapp.views;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.openyoung.globant.chatbotapp.R;
import com.openyoung.globant.chatbotapp.adapters.ImageAdapters;
import com.openyoung.globant.chatbotapp.listeners.AIListener;
import com.openyoung.globant.chatbotapp.models.Message;
import com.openyoung.globant.chatbotapp.models.Payload;
import com.openyoung.globant.chatbotapp.utils.ChatRequestAsyncTask;

import java.util.ArrayList;

import ai.api.model.AIError;
import ai.api.model.AIOutputContext;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private View button;
    private EditText editText;
    private ArrayList<AIOutputContext> aiContext;
    private boolean isFirst = true;

    private boolean TRACK_NAME;
    private String USER_NAME;
    private ScrollView _scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);
        initUI();
    }

    LayoutInflater layoutInflater;
    LinearLayout linearLayout;

    private void initUI() {
        linearLayout = (LinearLayout) findViewById(R.id.layoutComponent);
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        button = findViewById(R.id.sendButton);
        button.setOnClickListener(this);
        editText = (EditText) findViewById(R.id.requestText);
        _scrollView=(ScrollView)findViewById(R.id.scrollView);
    }


    private void initChat(String requestChat) {
        AIRequest aiRequest = new AIRequest();
        aiRequest.setQuery(requestChat);
        executeChatRequest(aiRequest);
    }

    private void executeChatRequest(AIRequest aiRequest) {
        AIRequest[] aiRequests = new AIRequest[]{aiRequest};
        ChatRequestAsyncTask asyncTask = new ChatRequestAsyncTask((AIListener) this, aiDataService);
        asyncTask.execute(aiRequests);
    }


    @Override
    public void onResult(AIResponse result) {
        Log.d(getClass().getSimpleName(), "@@@@: " + result.toString());
        Payload payload = getPayload(result);
        if (null != payload) {
           /* addUserName(payload.getMessage());
            checkAndStoreUserName(payload.getMessage());*/
            addUIOperations(payload);
        }

    }

    private void addUserName(String message) {
        if (message.contains("<USER>")) {
            //TODO:: replace user name with this
        }
    }

    private void setAIContext(AIResponse result) {
        if (null != result && null != result.getResult().getContexts() && result.getResult().getContexts().size() > 0) {
            this.aiContext = (ArrayList<AIOutputContext>) (result.getResult().getContexts());
        } else {
            this.aiContext = null;
        }
    }

    /**
     *
     */
    private void addUIOperations(Payload payload) {
        Log.d(getClass().getSimpleName(), "Payload is::" + payload.toString());
        try {
            Log.d(getClass().getSimpleName(), "addUIOperations is " + payload.toString());
            if (payload.getMessageType().equalsIgnoreCase("text")) {
                addTextView(payload.getMessage());
                return;
            } else if (payload.getMessageType().equalsIgnoreCase("links")) {
                addImageRecyclerView(payload.getMessage());
                return;
            } else if (payload.getMessageType().equalsIgnoreCase("text-links")) {
                addTextView(payload.getMessage());
                addImageRecyclerView(payload.getMessage());
                return;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void checkAndStoreUserName(String message) {
        if (message.contains("<USER>")) {
            TRACK_NAME = true;
        } else {
            TRACK_NAME = false;
        }
    }

    private void addImageView(String message) {
        Log.d(getClass().getSimpleName(), "addTextView is " + message);
        ScrollView scrollView = getImageListView(R.layout.image_links_layout);
        linearLayout.addView(scrollView);
    }

    private void addImageRecyclerView(String message) {
        Log.d(getClass().getSimpleName(), "addTextView is " + message);
        View scrollView = getImageRecyclerView(message);
        linearLayout.addView(scrollView);
    }

    private ScrollView getImageListView(int layout) {
        return (ScrollView) layoutInflater.inflate(layout, null);
    }


    private TextView getTextView(int layout) {
        return (TextView) layoutInflater.inflate(layout, null);
    }

    private View inflateView(int layout) {
        return layoutInflater.inflate(layout, null);
    }

    private void addTextView(String text) {
        Log.d(getClass().getSimpleName(), "addTextView is " + text);
        View view = inflateView(R.layout.textview_chat);
        TextView textView = (TextView) view.findViewById(R.id.txtItem);
        textView.setText(text);
        linearLayout.addView(view);
    }

    private void addRequestChatTextView(String text) {
        if (TRACK_NAME) {
            USER_NAME = text.trim();
        }
        Log.d(getClass().getSimpleName(), "addTextView is " + text);
        View view = inflateView(R.layout.textview_request_chat);
        TextView textView = (TextView) view.findViewById(R.id.txtItem);
        textView.setText(text);
        linearLayout.addView(view);
    }

    private Payload getPayload(AIResponse response) {
        try {
            if (!TextUtils.isEmpty(response.getResult().getFulfillment().getSpeech())) {
                Payload payload = new Payload();
                payload.setMessage(response.getResult().getFulfillment().getSpeech());
                payload.setComponent("text");
                payload.setMessageType("text");
                return payload;
            } else {
                Gson gson = new Gson();
                final String jsonString = gson.toJson(response.getResult().getFulfillment().getMessages().get(0));
                Message message = gson.fromJson(jsonString, Message.class);
                Log.i(getClass().getSimpleName(), "Message is " + message.toString());
                return message.getPayload();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public void onError(AIError error) {
        Log.d(getClass().getSimpleName(), "@@@@##: " + error.getMessage());
    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == button.getId()) {
            String strRequest = editText.getText().toString();
            if (!TextUtils.isEmpty(strRequest)) {
                addRequestChatTextView(strRequest);
                _scrollView.fullScroll(View.FOCUS_DOWN);
                initChat(strRequest);
                editText.setText("");
                hideKeyboard(editText);
            }
        }
    }

    public void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private View getImageRecyclerView(String message) {
        View view = (View) layoutInflater.inflate(R.layout.recyclerview_chat, null);
        RecyclerView horizontal_recycler_view = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);
        ArrayList<Integer> horizontalList = getDataList(message);
        ImageAdapters horizontalAdapter = new ImageAdapters(horizontalList, this);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
        horizontal_recycler_view.setAdapter(horizontalAdapter);
        return view;
    }


    private ArrayList<Integer> getDataList(String message) {
        int size = 3;
        ArrayList<Integer> horizontalList = new ArrayList<Integer>();
        if (message.contains(";")) {

            getDataList(size, horizontalList,message );
        } else {
            horizontalList.add(R.drawable.expenditure);
        }
        return horizontalList;
    }

    private void getDataList(int size, ArrayList<Integer> horizontalList, String message) {
        for (int i = 0; i < size; i++) {
            switch (i) {
                case 0:
                    if(message.contains("capability;capability;capability")){
                        horizontalList.add(R.drawable.one);
                    }else {
                        horizontalList.add(R.drawable.four);
                    }
                    break;
                case 1:
                    if(message.contains("capability;capability;capability")){
                        horizontalList.add(R.drawable.two);
                    }else {
                        horizontalList.add(R.drawable.five);
                    }

                    break;
                case 2:
                    if(message.contains("capability;capability;capability")){
                        horizontalList.add(R.drawable.three);
                    }else if(message.contains("offer;offer;offer")){
                        horizontalList.add(R.drawable.six);
                    }
                    break;
            }

        }
    }
}
