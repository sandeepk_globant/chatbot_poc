package com.openyoung.globant.chatbotapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.openyoung.globant.chatbotapp.R;

/**
 * Created by sandeep.kharat on 8/22/2017.
 */

public class ImageHolder extends RecyclerView.ViewHolder {
    public static ImageView imageViewIcon;
    public ImageHolder(View itemView) {
        super(itemView);
        this.imageViewIcon = (ImageView) itemView.findViewById(R.id.single_image);
    }
}
