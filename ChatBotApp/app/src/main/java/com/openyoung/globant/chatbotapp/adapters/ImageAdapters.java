package com.openyoung.globant.chatbotapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.openyoung.globant.chatbotapp.R;

import java.util.ArrayList;

/**
 * Created by sandeep.kharat on 8/22/2017.
 */

public class ImageAdapters extends RecyclerView.Adapter<ImageHolder> {

    private final ArrayList<Integer> idsList;
    private final Context context;

    public ImageAdapters(ArrayList<Integer> idsList, Context context) {
        this.idsList = idsList;
        this.context = context;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent,
                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.imageview_single_chat, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWebActivity();
            }
        });

        ImageHolder myViewHolder = new ImageHolder(view);
        return myViewHolder;
    }

    private void startWebActivity() {
        String url = "http://www.santander.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    @Override
    public void onBindViewHolder(final ImageHolder holder, final int listPosition) {
        ImageView imageView = holder.imageViewIcon;
        imageView.setImageResource(this.idsList.get(listPosition));
    }

    @Override
    public int getItemCount() {
        return idsList.size();
    }
}
